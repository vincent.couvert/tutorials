# Scilab tutorials project

This collaborative project contains tutorials about Scilab from basic to advanced features.

Most of these tutorials are Jupyter Notebooks you will be able to re-play and modify for a better understanding of what you can do with Scilab.

## Table of contents

Here is the list of the tutorials available in this project:

- [Introduction about Scilab](docintrotoscilab/overview.ipynb): good start if you are a Scilab beginner,
- [Tutorial about Scilab graphics](figures_in_scilab/figures_in_scilab.ipynb): guide on how to use Scilab graphics properties,
- [Start with Scilab and Jupyter](jupyter_introduction/Scilab%20jupyter%20introduction.ipynb): installation guide,
- [Introduction about timeseries](timeseries/timeseries.ipynb): see how to manage your data using `timeseries` in Scilab.

Feel free to contribute...
