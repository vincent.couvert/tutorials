{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "288e187d-7e02-4769-94f6-0132e141c2ed",
   "metadata": {},
   "source": [
    "# Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10fd7abf-5ddb-4cd9-b96e-96967256b18b",
   "metadata": {},
   "source": [
    "## Table of contents\n",
    "1. [Overview](#functions_overview)\n",
    "2. [Defining a function](#functions_defining)\n",
    "3. [Managing output arguments](#functions_output)\n",
    "4. [Levels in the call stack](#functions_callstack)\n",
    "5. [The **return** statement](#functions_return)\n",
    "6. [Debugging functions with **pause**](#functions_pause)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b413f41-ee7b-4171-a866-813d4406cfcb",
   "metadata": {},
   "source": [
    "In this section, we present Scilab functions. We analyze the way to define a new function and the method\n",
    "to load it into Scilab. We present how to create and load a **library**, which is a collection of functions. We also present how to manage input and output arguments. Finally, we present how to debug a function using the <span style=\"color:red\">**pause**</span> statement."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3abf3caf-cd30-4460-b74f-3cdb2565f82b",
   "metadata": {},
   "source": [
    "## Overview <a name=\"functions_overview\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33921e9d-ec03-478d-a279-bcf3cbb1f677",
   "metadata": {},
   "source": [
    "Gathering various steps into a reusable function is one of the most common tasks of a Scilab developer. \n",
    "The most simple calling sequence of a function is the following: `outvar = myfunction ( invar )` \n",
    "where the following list presents the various variables used in the syntax:\n",
    "\n",
    "- **myfunction** is the name of the function,\n",
    "- **invar** is the name of the input arguments,\n",
    "- **outvar** is the name of the output arguments.\n",
    "\n",
    "The values of the input arguments are not modified by the function, while the \n",
    "values of the output arguments are actually modified by the function.\n",
    "\n",
    "We have in fact already met several functions in this document. The <span style=\"color:red\">**sin**</span> function, in the **y=sin(x)** statement, \n",
    "takes the input argument **x** and returns the result in the output argument **y**.\n",
    "In Scilab vocabulary, the input arguments are called the **right hand side** and the output arguments are called the **left hand side**.\n",
    "\n",
    "Functions can have an arbitrary number of input and output arguments so that the complete syntax for a function which has a fixed number of arguments is the following: `[o1, ..., on] = myfunction ( i1, ..., in )`\n",
    "The input and output arguments are separated by commas **,**.\n",
    "Notice that the input arguments are surrounded by opening and closing parentheses,\n",
    "while the output arguments are surrounded by opening and closing **square** brackets.\n",
    "\n",
    "In the following Scilab session, we show how to compute the $LU$ decomposition of the Hilbert matrix. The following session shows how to create a matrix with the <span style=\"color:red\">**testmatrix**</span> function, which takes two input arguments, and returns one matrix. Then, we use the <span style=\"color:red\">**lu**</span> function, which takes one input argument and returns two or three arguments depending on the provided output variables. If the third argument **P** is provided, the permutation matrix is returned.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "ce111290-32da-469f-8176-726fe7d641fc",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " A  = \n",
      "   4.  -6. \n",
      "  -6.   12.\n",
      " L  = \n",
      "  -0.6666667   1.\n",
      "   1.          0.\n",
      " U  = \n",
      "  -6.   12.\n",
      "   0.   2. \n",
      " L  = \n",
      "   1.          0.\n",
      "  -0.6666667   1.\n",
      " U  = \n",
      "  -6.   12.\n",
      "   0.   2. \n",
      " P  = \n",
      "   0.   1.\n",
      "   1.   0.\n"
     ]
    }
   ],
   "source": [
    "A = testmatrix(\"hilb\",2)\n",
    "[L, U] = lu(A)\n",
    "[L, U, P] = lu(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "accca3d3-c762-4616-b663-41569472dbcf",
   "metadata": {},
   "source": [
    "Notice that the behavior of the <span style=\"color:red\">**lu**</span> function actually changes when three output arguments are provided: the two rows of the matrix **L** have been swapped. More specifically, when two output arguments are provided, the decomposition $A=LU$ is provided (the statement **A-L*U** checks this). When three output arguments are provided, permutations are performed so that the decomposition $PA=LU$ is provided (the statement **P*A-L*U** can be used to check this). In fact, when two output arguments are provided, the permutations are applied on the **L** matrix. This means that the <span style=\"color:red\">**lu**</span> function knows how many input and output arguments are provided to it, and changes its algorithm accordingly. We will not present in this document how to provide this feature, i.e. a variable number of input or output arguments. But we must keep in mind that this is possible in the Scilab language.\n",
    "\n",
    "The commands provided by Scilab to manage functions are:\n",
    "\n",
    "- <span style=\"color:red\">**function**</span>: opens a function definition \n",
    "- <span style=\"color:red\">**endfunction**</span>: closes a function definition \n",
    "- <span style=\"color:red\">**argn**</span>: number of input/output arguments in a function call \n",
    "- <span style=\"color:red\">**varargin**</span>: variable numbers of arguments in an input argument list \n",
    "- <span style=\"color:red\">**varargout**</span>: variable numbers of arguments in an output argument list \n",
    "- <span style=\"color:red\">**get_function_path**</span>: get source file path of a library function \n",
    "- <span style=\"color:red\">**getd**</span>: getting all functions defined in a directory \n",
    "- <span style=\"color:red\">**head_comments**</span>: display Scilab function header comments \n",
    "- <span style=\"color:red\">**macrovar**</span>: variables of function of function \n",
    "\n",
    "In the next sections, we will present some of the most commonly used commands."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "856ede3d-871e-4626-993a-97a1c4c6be3d",
   "metadata": {},
   "source": [
    "## Defining a function <a name=\"functions_defining\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f5e083f0-5205-41b9-9714-731d0a4c8677",
   "metadata": {},
   "source": [
    "To define a new function, we use the <span style=\"color:red\">**function**</span> and <span style=\"color:red\">**endfunction**</span> Scilab keywords. In the following example, we define the function **myfunction**, which takes the input argument **x**, multiplies it by 2, and returns the value in the output argument **y**.\n",
    "\n",
    "```\n",
    "function y = myfunction ( x )\n",
    "  y = 2 * x\n",
    "endfunction\n",
    "```\n",
    "\n",
    "The statement **function y = myfunction ( x )** is the **header** of the function while the **body** of the function is made of the statement **y = 2 * x**. The body of a function may contain one, two or more statements.\n",
    "\n",
    "There are at least three possibilities to define the previous function in Scilab.\n",
    "\n",
    "- The first solution is to type the script directly into the console in an interactive mode. Notice that, once the \"**function y = myfunction ( x )**\" statement has been written and the enter key is typed in, Scilab creates a new line in the console, waiting for the body of the function.\n",
    "When the \"**endfunction**\" statement is typed in the console, Scilab returns back to its normal edition mode. \n",
    "\n",
    "- Another solution is available when the source code of the function is provided in a file. This is the most common case, since functions are generally quite long and complicated. We can simply copy and paste \n",
    "the function definition into the console. When the function definition is short (typically, a dozen lines of source code), this way is very convenient. With the editor, this is very easy, thanks to the **Load into Scilab** feature.\n",
    "\n",
    "- We can also use the <span style=\"color:red\">**exec**</span> function. The <span style=\"color:red\">**exec**</span> function executes the content of the file as if it were written interactively in the console and displays the various Scilab statements, line after line. The file may contain a lot of source code so that the output may be very long and useless. In these situations, we add the semicolon character **;** at the end of the line.\n",
    "This is what is performed by the **Execute file with no echo** feature of the editor.\n",
    "```\n",
    "-->exec(path);\n",
    "```\n",
    "Notice that the previous function sets the value of the output argument **y**, with the statement **y=2*x**. This is mandatory. In order to see it, we define in the following script a function which sets the variable **z**, but not the output argument **y**.\n",
    "```\n",
    "function y = myfunction ( x )\n",
    "  z = 2 * x\n",
    "endfunction\n",
    "```\n",
    "\n",
    "In the following session, we try to use our function with the input argument **x=1**.\n",
    "```\n",
    "-->myfunction ( 1 )\n",
    " !--error 4 \n",
    "Undefined variable: y\n",
    "at line       4 of function myfunction called by :  \n",
    "myfunction ( 1 )\n",
    "```\n",
    "\n",
    "Indeed, the interpreter tells us that the output variable **y** has not been defined. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0014cdf-5d1d-459a-9284-086df5cae640",
   "metadata": {},
   "source": [
    "## Managing output arguments <a name=\"functions_output\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d1daa5b2-c8e4-455e-8123-ef1c4c77ea75",
   "metadata": {},
   "source": [
    "In this section, we present the various ways to manage output arguments. A function may have zero or more input and/or output arguments. In the most simple case, the number of input and output arguments is pre-defined and using such a function is easy. But, as we are going to see, even such a simple function can be called in various ways.\n",
    "\n",
    "Assume that the function <span style=\"color:red\">**simplef**</span> is defined with 2 input arguments and 2 output arguments, as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "65115a0e-64a3-449d-8ead-cff8a8cdc5a9",
   "metadata": {},
   "outputs": [],
   "source": [
    "function [y1 , y2] = simplef ( x1, x2 )\n",
    "  y1 = 2 * x1\n",
    "  y2 = 3 * x2\n",
    "endfunction"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "75d04c39-5bb0-4b63-b303-efc5ab3a45be",
   "metadata": {},
   "source": [
    "In fact, the number of output arguments of such a function can be 0, 1 or 2. When there is no output argument, the value of the first output argument in stored in the <span style=\"color:red\">**ans**</span> variable. We may also set the variable **y1** only. Finally, we may use all the output arguments, as expected. The following session presents all these calling sequences."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "d810317f-48a1-49b5-a73e-55e4c2278b76",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " ans  =\n",
      "   2.\n",
      " y1  = \n",
      "   2.\n",
      " y1  = \n",
      "   2.\n",
      " y2  = \n",
      "   6.\n"
     ]
    }
   ],
   "source": [
    "simplef ( 1 , 2 )\n",
    "y1 = simplef ( 1 , 2 )\n",
    "[y1,y2] = simplef ( 1 , 2 )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "118fedc2-a67f-414f-bde6-482c833e7b24",
   "metadata": {},
   "source": [
    "We have seen that the most basic way of defining functions already allows to manage a variable number of output arguments. There is an even more flexible way of managing a variable number of input and output arguments, based on the <span style=\"color:red\">**argn**</span>, <span style=\"color:red\">**varargin**</span> and <span style=\"color:red\">**varargout**</span> variables. This more advanced topic will not be detailed in this document."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aa05696a-549b-4190-bc3f-2be203c4bf6b",
   "metadata": {},
   "source": [
    "## Levels in the call stack <a name=\"functions_callstack\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "43a53c03-d085-46f6-8c65-74b3fb1b8b2d",
   "metadata": {},
   "source": [
    "Obviously, function calls can be nested, i.e. a function **f** can call a function **g**, which in turn calls a function **h** and so forth. When Scilab starts, the variables which are defined are at the **global** scope. When we are in a function which is called from the global scope, we are one level down \n",
    "in the call stack. When nested function calls occur, the current level in the call stack is equal to the number of previously nested calls. The functions inquire about the state of the call stack:\n",
    "- <span style=\"color:red\">**whereami**</span> displays current instruction calling tree\n",
    "- <span style=\"color:red\">**where**</span> gets current instruction calling tree\n",
    "\n",
    "In the following session, we define 3 functions which are calling one another and we use the function <span style=\"color:red\">**whereami**</span> to display the current instruction calling tree."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "2b36f4da-ebfc-46ed-8d88-2bfa1096d1e9",
   "metadata": {},
   "outputs": [],
   "source": [
    "function y = fmain ( x )\n",
    "  y = 2 * flevel1 ( x )\n",
    "endfunction\n",
    "function y = flevel1 ( x )\n",
    "  y = 2 * flevel2 ( x )\n",
    "endfunction\n",
    "function y = flevel2 ( x )\n",
    "  y = 2 * x\n",
    "  whereami()\n",
    "endfunction"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f340921-a2e4-418d-a1c4-388445622a14",
   "metadata": {},
   "source": [
    "When we call the function **fmain**, the following output is produced. As we can see, the 3 levels in the call stack are displayed, associated with the corresponding function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "5eb04220-9978-41c7-91fc-f7e860a29d96",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "whereami called at line 3 of macro flevel2\n",
      "flevel2  called at line 2 of macro flevel1\n",
      "flevel1  called at line 2 of macro fmain\n",
      " ans  =\n",
      "   8.\n"
     ]
    }
   ],
   "source": [
    "fmain(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25ef6418-9127-48c3-b005-170c41de2ce3",
   "metadata": {},
   "source": [
    "In the previous example, the various calling levels are the following:\n",
    "\n",
    "- level 0 : the global level,\n",
    "- level -1 : the body of the **fmain** function,\n",
    "- level -2 : the body of the **flevel1** function,\n",
    "- level -3 : the body of the **flevel2** function.\n",
    "\n",
    "These calling levels are displayed in the prompt of the console when we interactively debug a function with the <span style=\"color:red\">**pause**</span> statement or with breakpoints."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9e5c3233-64ae-4c45-8792-9ca598cb131c",
   "metadata": {},
   "source": [
    "## The **return** statement <a name=\"functions_return\" />"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "010166cc-45b0-449c-b149-916209a0431d",
   "metadata": {},
   "source": [
    "Inside the body of a function, the <span style=\"color:red\">**return**</span> statement immediately stops the function, i.e. it immediately quits the current function. This statement can be used in cases where the remaining of the algorithm is not necessary.\n",
    "\n",
    "The following function computes the sum of integers from **istart** to **iend**. In regular situations,\n",
    "it uses the <span style=\"color:red\">**sum**</span> function to perform its job. But if the **istart** variable is negative or if the \n",
    "**istart<=iend** condition is not satisfied, the output variable **y** is set to 0 and the function immediately returns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "c940f74a-15a5-45c5-9364-bec9f92988c2",
   "metadata": {},
   "outputs": [],
   "source": [
    "function y = mysum ( istart , iend )\n",
    "  if ( istart < 0 ) then\n",
    "    y = 0\n",
    "    return\n",
    "  end\n",
    "  if ( iend < istart ) then\n",
    "    y = 0\n",
    "    return\n",
    "  end\n",
    "  y = sum ( istart : iend )\n",
    "endfunction"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b937f168-83d1-43ba-b970-a719c35b90d2",
   "metadata": {},
   "source": [
    "The following session checks that the <span style=\"color:red\">**return**</span> statement is correctly used by the **mysum** function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "9773ca86-f21f-44f6-9d55-0f25a64797e8",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " ans  =\n",
      "   15.\n",
      " ans  =\n",
      "   0.\n",
      " ans  =\n",
      "   0.\n"
     ]
    }
   ],
   "source": [
    "mysum ( 1 , 5 )\n",
    "mysum(-1, 5)\n",
    "mysum(2, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17e88e6d-25c6-48ef-80b6-23119542a608",
   "metadata": {},
   "source": [
    "Some developers state that using several <span style=\"color:red\">**return**</span> statements in a function is generally a bad practice. \n",
    "Indeed, we must take into account the increased difficulty of debugging such a function, because the algorithm may suddenly quit the body of the function. The user may get confused about what exactly caused the function to return.\n",
    "\n",
    "This is why, in practice, the <span style=\"color:red\">**return**</span> statement should be used with care, and certainly not in every function. The rule to follow is that the function should return only at its very last line.\n",
    "Still, in particular situations, using <span style=\"color:red\">**return**</span> can actually greatly simplify the algorithm, while avoiding <span style=\"color:red\">**return**</span> would require writing a lot of unnecessary source code."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72defe36-0293-4828-8931-ac8c90c39f54",
   "metadata": {},
   "source": [
    "## Debugging functions with **pause** <a name=\"functions_pause\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8f47402-e276-4b4e-9621-a7d1a43ed5e9",
   "metadata": {},
   "source": [
    "In this section, we present simple debugging methods that fix most simple bugs in a convenient and efficient way which are:\n",
    "\n",
    "- <span style=\"color:red\">**pause**</span> waits for interactive user input \n",
    "- <span style=\"color:red\">**resume**</span> resumes execution and copy some local variables \n",
    "- <span style=\"color:red\">**abort**</span> interrupts evaluation \n",
    "\n",
    "A Scilab session usually consists in the definition of new algorithms by the creation of new functions. \n",
    "It often happens that a syntax error or an error in the algorithm produces a wrong result.\n",
    "\n",
    "Consider the problem, the sum of integers from **istart** to **iend**. Again, this simple example is chosen for demonstration purposes, since the <span style=\"color:red\">**sum**</span> function performs it directly.\n",
    "\n",
    "The following function **mysum** contains a bug: the second argument \"**foo**\" passed to the <span style=\"color:red\">**sum**</span> function has no meaning in this context."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "d97eb8b3-4792-42b9-893f-ee5448f896e3",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Warning : redefining function: mysum                   . Use funcprot(0) to avoid this message\n"
     ]
    }
   ],
   "source": [
    "function y = mysum ( istart , iend )\n",
    "  y = sum ( iend : istart , \"foo\" )\n",
    "endfunction"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2756413-8c9c-46c3-a1a8-5427b2ec9a2d",
   "metadata": {},
   "source": [
    "The following session shows what happens when we use the **mysum** function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "7ff75b9c-fa55-41e2-a755-1fae416b0fe8",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "at line     2 of function mysum \n",
      "\n",
      "sum: Wrong value for input argument #2: Must be in the set {\"*\",\"r\",\"c\",\"m\",\"native\",\"double\"}.\n"
     ]
    }
   ],
   "source": [
    "mysum ( 1 , 10 )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e0a4c33-e835-4afe-9ac8-6b399c36659f",
   "metadata": {},
   "source": [
    "In order to interactively find the problem, we place a <span style=\"color:red\">**pause**</span> statement inside the body of the function.\n",
    "```\n",
    "function y = mysum ( istart , iend )\n",
    "  pause\n",
    "  y = sum ( iend : istart , \"foo\" )\n",
    "endfunction\n",
    "```\n",
    "\n",
    "We now call the function **mysum** again with the same input arguments.\n",
    "\n",
    "```\n",
    "-->mysum ( 1 , 10 )\n",
    "Type 'resume' or 'abort' to return to standard level prompt.\n",
    "-1->\n",
    "```\n",
    "\n",
    "We are now interactively located **in the body** of the **mysum** function.\n",
    "The prompt \"**-1->**\" indicates that the current call stack is at level -1.\n",
    "We can check the value of the variables **istart** and **iend** by simply typing their names in the console. \n",
    "\n",
    "```\n",
    "-1->istart\n",
    " istart  =\n",
    "    1.  \n",
    "-1->iend\n",
    " iend  =\n",
    "    10.  \n",
    "```\n",
    "\n",
    "In order to progress in our function, we can copy and paste the statements and see what happens interactively, as in the following session.\n",
    "\n",
    "```\n",
    "-1->y = sum ( iend : istart , \"foo\" )\n",
    "y = sum ( iend : istart , \"foo\" )\n",
    "                                  !--error 44 \n",
    "Wrong argument 2.\n",
    "```\n",
    "\n",
    "We can see that the call to the <span style=\"color:red\">**sum**</span> function does not behave how we might expect. The \"**foo**\" input argument is definitely a bug: we remove it. \n",
    "\n",
    "```\n",
    "-1->y = sum ( iend : istart )\n",
    " y  =\n",
    "    0.  \n",
    "```\n",
    "\n",
    "After the first revision, the call to the <span style=\"color:red\">**sum**</span> function is now syntactically correct. But the result is still wrong, since the expected result in this case is 55. We see that the **istart** and **iend** variables have been **swapped**. We correct the function call and check that the fixed version behaves as expected\n",
    "\n",
    "```\n",
    "-1->y = sum ( istart : iend )\n",
    " y  =\n",
    "    55.  \n",
    "```\n",
    "\n",
    "The result is now correct. In order to get back to the zero level, we now use the <span style=\"color:red\">**abort**</span> statement, which interrupts the sequence and immediately returns to the global level.\n",
    "\n",
    "```\n",
    "-1->abort\n",
    "-->\n",
    "```\n",
    "\n",
    "The `-->` prompt confirms that we are now back at the zero level in the call stack.\n",
    "\n",
    "We fix the function definition, which becomes: \n",
    "\n",
    "```\n",
    "function y = mysum ( istart , iend )\n",
    "  pause\n",
    "  y = sum ( istart : iend )\n",
    "endfunction\n",
    "```\n",
    "\n",
    "In order to check our bugfix, we call the function again.\n",
    "\n",
    "```\n",
    "-->mysum ( 1 , 10 )\n",
    "Type 'resume' or 'abort' to return to standard level prompt.\n",
    "-1->\n",
    "```\n",
    "\n",
    "We are now confident about our code, so that we use the <span style=\"color:red\">**resume**</span> statement, which lets Scilab execute the code as usual.\n",
    "\n",
    "```\n",
    "-->mysum ( 1 , 10 )\n",
    "-1->resume\n",
    " ans  =\n",
    "    55.  \n",
    "```\n",
    "\n",
    "The result is correct. All we have to do is to remove the <span style=\"color:red\">**pause**</span> statement from the function definition.\n",
    "\n",
    "```\n",
    "function y = mysum ( istart , iend )\n",
    "  y = sum ( istart : iend )\n",
    "endfunction\n",
    "```\n",
    "\n",
    "In this section, we have seen that, used in combination, the <span style=\"color:red\">**pause**</span>, <span style=\"color:red\">**resume**</span> and <span style=\"color:red\">**abort**</span> statements are a very effective way to interactively debug a function. In fact, our example is very simple and the method we presented may appear to be too simple to be convenient. This is not the case. \n",
    "In practice, the <span style=\"color:red\">**pause**</span> statement has proved to be a very fast way to find and fix bugs, even in very complex situations."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Scilab",
   "language": "scilab",
   "name": "scilab"
  },
  "language_info": {
   "file_extension": ".sci",
   "help_links": [
    {
     "text": "MetaKernel Magics",
     "url": "https://metakernel.readthedocs.io/en/latest/source/README.html"
    }
   ],
   "mimetype": "text/x-scilab",
   "name": "scilab",
   "version": "0.10.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
