{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "eac7bdbd-82d4-44f6-8c72-e6d43722ee08",
   "metadata": {},
   "source": [
    "# First Steps"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e05edc66-43e5-4a88-8b35-60ca74cd38ec",
   "metadata": {},
   "source": [
    "# Table of contents\n",
    "1. [The console](#console)\n",
    "2. [The editor](#editor)\n",
    "3. [The variable browser and the command history](#browser)\n",
    "4. [Docking](#docking)\n",
    "6. [Using exec](#scilab_exec)\n",
    "7. [Help](#scilab_help)\n",
    "8. [Demonstration](#scilab_demo)\n",
    "9. [ATOMS, the packaging system of Scilab](#scilab_atoms)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40f842df-8e48-469f-acde-33c7032c232c",
   "metadata": {},
   "source": [
    "In this section, we make our first steps with Scilab and present some simple tasks we can perform with the interpreter.  \n",
    "There are several ways of using Scilab. Here, we present the method using the console in the interactive mode. \n",
    "\n",
    "We also present the management of the graphical windows with the docking system. Finally, we present three features of Scilab: \n",
    "- the embedded help,\n",
    "- the demonstrations,\n",
    "- ans the ATOMS system, a packaging system for external modules."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94c7c813-6da1-4760-969b-c2edacce7ba9",
   "metadata": {},
   "source": [
    "## The console <a name=\"console\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "46bc29d0-4376-4fb8-93c1-1b77b033fc1f",
   "metadata": {},
   "source": [
    "The first way is to use Scilab interactively, by typing commands in the console, analyzing the results and continuing this process until the final result is computed. This document is designed so that the Scilab examples which are executed here or copied into your console.  \n",
    "\n",
    "After double-clicking the icon to launch Scilab, Scilab environment by default consists of the following docked windows:\n",
    "- console,\n",
    "- files browser,\n",
    "- variables browser,\n",
    "- command history,\n",
    "- news feed.\n",
    "\n",
    "![scilab-2024.0.0.png](images/scilab-2024.0.0.png)\n",
    "\n",
    "In the console after the **prompt** **-->**, just type a command and press the **< Enter >** key (Windows and Linux) or **< Return >** key (Mac OS X) on the keyboard to obtain the corresponding result.\n",
    "\n",
    "In the following example, the function <span style=\"color:red\">**disp**</span> is used in the interactive mode to print out the string \"Hello World!\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "726f90cd-e4c2-4380-b366-3fceccb9e9cb",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " s  = \n",
      "  \"Hello World!\"\n",
      "\n",
      "  \"Hello World!\"\n"
     ]
    }
   ],
   "source": [
    "s = \"Hello World!\"\n",
    "disp(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37e093c9-fbf5-43d4-866b-df7359b8b513",
   "metadata": {},
   "source": [
    "When we edit a command, we can use the keyboard, as with a regular editor. We can use the left $\\leftarrow$ and right $\\rightarrow$ arrows in order to move the cursor on the line and use the **< Backspace >** and **< Suppr >** keys in order to fix errors in the text.\n",
    "\n",
    "In order to get access to previously executed commands, we use the up arrow $\\uparrow$ key. This lets us browse the previous \n",
    "commands by using the up $\\uparrow$ and down $\\downarrow$ arrow keys. \n",
    "\n",
    "The **< Tab >** key provides a very convenient completion feature. In the following session, we type the statement <span style=\"color:red\">**disp**</span> in the console. Then we can type on the **< Tab >** key, which makes a list appear in the console.\n",
    "\n",
    "![completion.png](images/completion.png)\n",
    "\n",
    "Scilab displays a listbox, where items correspond to all functions which begin with the letters \"disp\". We can then use the up and down arrow keys to select the function we want.\n",
    "\n",
    "The auto-completion works with functions, variables, files and graphic handles and makes the development of scripts easier and faster.  \n",
    "\n",
    "Other example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "ca83b912-c4b0-4819-a189-d97a55a01221",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " ans  =\n",
      "   14.25\n",
      " ans  =\n",
      "   11.\n"
     ]
    }
   ],
   "source": [
    "57 / 4\n",
    "2 + 9"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4bbb962-7ec0-4fe4-ad6f-df26aec7d992",
   "metadata": {},
   "source": [
    "**Mention**: Before the result <span style=\"color:red\">**ans**</span> is displayed for \"**answer**\"."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d7b56957-8ed0-4df2-a37d-b480d1a753d2",
   "metadata": {},
   "source": [
    "## The editor <a name=\"editor\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0e1c96a1-36f5-4e40-8cf3-18ae0199cc43",
   "metadata": {},
   "source": [
    "Typing directly into the console has two disadvantages:\n",
    "- it is not possible to save the commands\n",
    "- and it is not easy to edit multiple lines of instruction.\n",
    "\n",
    "So, Scilab provides an editor for editing scripts easily. \n",
    "\n",
    "![scinotes.png](images/scinotes.png)\n",
    "\n",
    "The editor is accessed \n",
    "- from the menu of the console, under the **Applications > SciNotes** menu, \n",
    "- from the console by <span style=\"color:red\">**scinotes**</span> function,\n",
    "- or the first icon in the toolbar console ![accessories-text-editor.png](images/accessories-text-editor.png)\n",
    "\n",
    "The editor opens with a default file named \"**Untitled 1**\".\n",
    "\n",
    "This editor manages several files at the same time where we edit five files at the same time. \n",
    "\n",
    "There are many features which are worth mentioning in this editor. The most commonly used features are under the **Execute** menu.\n",
    "- Execute **...file with no echo** (Ctrl+Shift+E under Windows and Linux, Cmd+Shift+E under Mac OS X): the file is executed without writing the program in the console (saving the file first is mandatory).\n",
    "- Execute **... file with echo** (Ctrl+L under Windows and Linux, Cmd+L under Mac OS X): rewrites the file into the console and executes it.\n",
    "- Execute **... the selection with echo** (Ctrl+E under Windows and Linux, Cmd+E under Mac OS X): rewrites the selection chosen with the mouse into the console and executes it or executes the file data until the caret position defined by the user.\n",
    "\n",
    "We can also select a few lines in the script, right click (or Cmd+Click under Mac), and get the context menu which is presented in following figure.\n",
    "\n",
    "![scinotes_right_click.png](images/scinotes_right_click.png)\n",
    "\n",
    "The **Edit** menu provides a very interesting feature, commonly known as a \"pretty printer\" in most languages. This is the **Edit > Correct Indentation** feature, which automatically indents the current selection. This feature is extremely convenient, because it formats algorithms, so that the **if**, **for** and other structured blocks are easy to analyze.\n",
    "\n",
    "The editor provides a fast access to the inline help. Indeed, assume that we have selected the <span style=\"color:red\">**disp**</span> statement. When we right-click in the editor, we get the context menu, where the **Help about \"disp\"** entry opens the help page associated with the <span style=\"color:red\">**disp**</span> function.\n",
    "\n",
    "![scinotes_help_display.png](images/scinotes_help_disp.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a242f90-a991-46ae-acf5-a40c802ffa8e",
   "metadata": {},
   "source": [
    "## The variable browser and the command history <a name=\"browser\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a350fdf6-7416-4372-80cb-10243c62b88b",
   "metadata": {},
   "source": [
    "Scilab provides a variable browser, which displays the list of variables currently used in the environment. \n",
    "\n",
    "![var_browser.png](images/var_browser.png)\n",
    "\n",
    "We can access this browser through the menu **Applications > Variable Browser**, but the function <span style=\"color:red\">**browsevar**</span> has the same effect. \n",
    "\n",
    "We can double click on a variable, which opens the variable editor. We can then interactively change the value of a variable by changing its content in a cell. On the other hand, if we change the variable content within the console, we must refresh the content of the dialog box by pushing the refresh button in the toolbar of the Variable Editor.\n",
    "\n",
    "![var_editor.png](images/var_editor.png)\n",
    "\n",
    "The Command History dialog allows browsing through the commands that we have previously executed. This dialog is available in the menu **Applications > Command History**.\n",
    "\n",
    "![commandhistory.png](images/commandhistory.png)\n",
    "\n",
    "We can select any command in the list and double-click on it to execute it in the console. The right-click opens a context menu which lets us evaluate the command \n",
    "or edit it in the editor."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "655da999-17a7-4ecd-bb93-988f66fe7f87",
   "metadata": {},
   "source": [
    "## Docking <a name=\"docking\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "985ed4b0-5d56-49d0-8ba1-5f86fd810252",
   "metadata": {},
   "source": [
    "The graphics in Scilab version 5 has been updated so that many components are now based on Java. This has a number of advantages, including the possibility to manage docking windows.\n",
    "\n",
    "As in the default Scilab environment, where the console, files and variables browsers and command history are all together docked windows in Scilab can be repositioned in a single one. For example, the user can choose to position the editor in the default environment of Scilab.\n",
    "\n",
    "To dock a window in another one, first identify the blue horizontal bar under Windows, or black under Mac OS X and Linux, at the top of the window in the toolbar containing a question mark on the right.\n",
    "- Under Windows and Linux, click on this bar with the left mouse button and, while maintaining the click, move the mouse pointer in the desired window.\n",
    "- Under MacOS X, click on this bar and while maintaining the click, move it the desired window.\n",
    "\n",
    "A rectangle appears indicating the future positioning of the window. When the position is the one you want, release the mouse button. To cancel and bring out the window, click on the small arrow on the right of the same bar.\n",
    "\n",
    "\n",
    "![docking.png](images/docking.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2475bbaf-b240-4164-a1cd-1668e08611a1",
   "metadata": {},
   "source": [
    "## Using exec <a name=\"scilab_exec\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7c76f51f-9664-4f5f-8e5b-9cc9ad4669f7",
   "metadata": {},
   "source": [
    "When several commands are to be executed, it may be more convenient to write these statements into a file with Scilab editor. To execute the commands located in such a file, the <span style=\"color:red\">**exec**</span> function can be used, followed by the name of the script. This file generally has the extension **.sce** or **.sci**, depending on its content: \n",
    "- files having the **.sci** extension contain Scilab functions and executing them loads the functions into Scilab environment (but does not execute them),\n",
    "- files having the **.sce** extension contain both Scilab functions and executable statements.\n",
    "\n",
    "Executing a **.sce** file has generally an effect such as computing several variables and displaying the results in the console, creating 2D plots, reading or writing into a file, etc...\n",
    "\n",
    "Assume that the content of the file **myscript.sce** is the following.\n",
    "```\n",
    "disp(\"Hello World !\")\n",
    "```\n",
    "\n",
    "In the Scilab console, we can use the <span style=\"color:red\">**exec**</span> function \n",
    "to execute the content of this script.\n",
    "```\n",
    "-->exec(\"myscript.sce\")\n",
    "-->disp(\"Hello World !\")\n",
    " Hello World !   \n",
    "```\n",
    "\n",
    "In practical situations, such as debugging a complicated algorithm, the interactive mode is used most of the time with a sequence of calls to the <span style=\"color:red\">**exec**</span> and <span style=\"color:red\">**disp**</span> functions."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5c78055-5d52-4db2-a4ff-6416aca16085",
   "metadata": {},
   "source": [
    "## Help <a name=\"scilab_help\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84af9314-7420-4329-8d49-303f2ff4289a",
   "metadata": {},
   "source": [
    "To access the online help,\n",
    "- click on **? > Scilab Help** in the menu bar\n",
    "- use <span style=\"color:red\">**help**</span> function\n",
    "- click on ![help-browser.png](images/help-browser.png)\n",
    "\n",
    "![help_scilab.png](images/help_scilab.png)\n",
    "\n",
    "Examples of use can be executed in Scilab and edited in SciNotes in using the associated buttons in the example framework.\n",
    "\n",
    "To get help with any function, type <span style=\"color:red\">**help**</span> in the console followed by the name of the appropriate function. For example \n",
    "```\n",
    "--> help cos\n",
    "```\n",
    "displays help for <span style=\"color:red\">**cos**</span> function."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e5a41245-d72e-4ec7-ac5e-53a160694de2",
   "metadata": {},
   "source": [
    "## Demonstrations <a name=\"scilab_demo\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cbdf7d83-0af0-496c-9a08-7922a3a6b6de",
   "metadata": {},
   "source": [
    "Scilab provides a collection of demonstration scripts, which are available \n",
    "- from the console, in the menu **? > Scilab Demonstrations**\n",
    "- from the icon toolbar ![x-office-presentation.png](images/x-office-presentation.png)\n",
    "\n",
    "![scilab_demo.png](images/scilab_demo.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "957e2fc1-78af-428d-ad80-4040c64ca375",
   "metadata": {},
   "source": [
    "## ATOMS, the packaging system of Scilab <a name=\"scilab_atoms\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6da399d3-ecbf-4ff7-a4c6-ae001101798a",
   "metadata": {},
   "source": [
    "In this section, we present ATOMS, which is a set of tools designed to install pre-built toolboxes.\n",
    "\n",
    "Scilab is designed to be extended by users, who can create new functions and use them as if they were distributed with Scilab. These extensions are called \"toolboxes\" or \"external modules\". The creation of a new module, with its associated help pages and unit tests, is relatively simple and this is a part of the success of Scilab.\n",
    "\n",
    "However, most modules cannot be used directly in source form: the module has to be compiled so that the binary files can be loaded into Scilab. This compilation step is not straightforward and may be even impossible for users who want to use a module based on C or Fortran source code and who do not have a compiler. This is one of the issues that ATOMS has solved: modules are provided in binary form, allowing the user to install a module without any compilation phase and without Scilab compatibilities issues.\n",
    "\n",
    "An extra feature for the user is that most modules are available on all platforms.\n",
    "\n",
    "ATOMS is the packaging system of Scilab external modules. With this tool, prebuilt (i.e. pre-compiled) Scilab modules, can be downloaded, installed and loaded. The dependencies are managed, so that if a module A depends on a module B, the installation of the module A automatically installs the module B. This is similar to the packaging system available in most GNU/Linux/BSD distributions. \n",
    "ATOMS modules are available on all operating systems on which Scilab is available, that is, on Microsoft Windows, GNU/Linux and Mac OS X.\n",
    "For example, when a ATOMS module is installed on Scilab running on a MS Windows operating system, the pre-built module corresponding to the\n",
    "MS Windows version of the module is automatically installed.\n",
    "The web portal for ATOMS is **[https://atoms.scilab.org](https://atoms.scilab.org)**.\n",
    "\n",
    "This portal presents the complete list of ATOMS modules and let the developers of the modules upload their new modules.\n",
    "\n",
    "There are two ways to install an ATOMS module. The first way is to use the <span style=\"color:red\">**atomsGui**</span> function, which opens a Graphical User Interface (GUI) which lets the user browse through all the available ATOMS modules. This tool is also available from the menu **\"Applications > Module manager - ATOMS\"** of the Scilab console. Within the GUI, we can read the description of the module and simply click on the **\"Install\"** button or from the icon of toolbar ![package-x-generic.png](images/package-x-generic.png)\n",
    "\n",
    "![scilab_atoms.png](images/scilab_atoms.png)\n",
    "\n",
    "The second way is to use the <span style=\"color:red\">**atomsInstall**</span> function, which takes the name of a module as input argument.  \n",
    "For example, to install the **scicv** module, the following statement should be executed:\n",
    "```\n",
    "--> atomsInstall(\"scicv\")\n",
    "```\n",
    "\n",
    "Then Scilab should be restarted and the **scicv** module (and, if any, its dependencies) are automatically loaded.\n",
    "\n",
    "More details on ATOMS are available at **[Wiki-Modules](https://scilab.gitlab.io/legacy_wiki/Modules.html)**. \n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Scilab",
   "language": "scilab",
   "name": "scilab"
  },
  "language_info": {
   "file_extension": ".sci",
   "help_links": [
    {
     "text": "MetaKernel Magics",
     "url": "https://metakernel.readthedocs.io/en/latest/source/README.html"
    }
   ],
   "mimetype": "text/x-scilab",
   "name": "scilab",
   "version": "0.10.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
