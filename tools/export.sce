notebookDirectories = [..
"docintrotoscilab/", ..
"figures_in_scilab/",..
"scicv_structural_analysis/"
]

rmdir("notebooks", "s")
rmdir("public", "s")

for notebookDirectory = notebookDirectories
    notebookFiles = pathconvert(listfiles(fullfile(notebookDirectory, "*.ipynb")), %F, %F, "u")'
    for notebookFile = notebookFiles
        
        [p, f, e] = fileparts(notebookFile);
    
        mprintf("Notebook: " + notebookFile + "\r\n")
        mprintf("Execute Notebook...\r\n")
        // --inplace --ClearMetadataPreprocessor.enabled=True --ClearMetadataPreprocessor.preserve_nb_metadata_mask=""kernelspec"" --ClearMetadataPreprocessor.preserve_nb_metadata_mask=""language_info""
        unix_w("jupyter nbconvert --to notebook --execute --output-dir notebooks/" + notebookDirectory + " " + notebookFile);
        
        notebookFile = fullfile("notebooks", notebookDirectory, f + e);
        
        mprintf("Convert Notebook to HTML...\r\n")
        host("jupyter nbconvert --to html --output-dir public/" + notebookDirectory + " " + notebookFile)
        
        mprintf("Convert Notebook to PDF...\r\n")
        // Fix images paths (must be absolute to be embedded in PDF)
        kJson = mgetl(notebookFile);
        iLine = 1
        while iLine < (size(kJson, "*") - 1)
            imageStart = strindex(kJson(iLine), "![")
            if imageStart <> [] then
                imagePart = part(kJson(iLine), imageStart:$);
                openPar = strindex(imagePart, "(")
                closePar = strindex(imagePart, ")")
                imagePath = part(imagePart, (openPar+1):(closePar-1))
                if strindex(imagePath, "attachment:") then
                    error("Invalid image path: " + imagePath)
                end
                if ~is_absolute_path(imagePath) then
                    kJson(iLine) = part(kJson(iLine), 1:(openPar+imageStart-1)) + strsubst(fullfile(pwd(), p, imagePath), "\", "/") + part(imagePart, closePar:$)
                end
            end
            iLine = iLine + 1
        end
        mputl(kJson, notebookFile);
        unix_w("jupyter nbconvert --to pdf --output-dir public/" + notebookDirectory + " " + notebookFile)
    
    end
    
    mkdir("public/" + notebookDirectory)
    copyfile("docintrotoscilab/images/", "public/" + notebookDirectory + "images/");
end

rmdir("notebooks", "s");
